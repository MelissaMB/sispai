package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Zona", catalog = "sgr")


public class Zona implements java.io.Serializable{
 private int idZona;
 private String nombre;

 public Zona() {
	 
	 
 }

public Zona(int idZona, String nombre) {
	
	this.idZona = idZona;
	this.nombre = nombre;
}
 
@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "zona_id_zona_seq")
@SequenceGenerator(name = "zona_id_zona_seq", sequenceName = "zona_id_zona_seq", allocationSize = 1)
@Column(name = "idZona", unique = true, nullable = false)
public int getIdZona() {
	return idZona;
}

public void setIdZona(int idZona) {
	this.idZona = idZona;
}

@Column(name = "nombre", nullable = false, length = 100)
public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}
 
 
		
 }