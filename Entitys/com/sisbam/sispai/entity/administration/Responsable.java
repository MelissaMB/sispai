package com.sisbam.sispai.entity.administration;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name = "responsable", catalog = "sgp")
public class Responsable implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;
	private int idResponsable;
	private String nombre;
	private String funcion;
	private String email;
	
	
	
	
	public Responsable() {
		
	}



	public Responsable(int idResponsable, String nombre, String funcion, String email) {
		
		this.idResponsable = idResponsable;
		this.nombre = nombre;
		this.email = email;
		this.funcion = funcion;
	}
	


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "responsable_idrecurso_seq")
	@SequenceGenerator(name = "responsable_idrecurso_seq", sequenceName = "responsable_idrecurso_seq", allocationSize = 1)
	@Column(name = "idResponsable", unique = true, nullable = false)
	public int getIdResponsable() {
		return idResponsable;
	}
	public void setIdResponsable(int idResponsable) {
		this.idResponsable = idResponsable;
	}
	


	@Column(name = "nombre", nullable = false, length = 256)
	public String getNombre() {
		return nombre;
	}
	
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "funcion", nullable = false, length = 256)
	public String getFuncion() {
		return funcion;
	}
	
	
	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}
	
	
	
	
	@Column(name = "email", nullable = true, length = 256)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

	}
	
	

