package com.sisbam.sispai.entity.administration;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "plan", catalog = "sgr")

public class Plan  implements java.io.Serializable{
	
	private int idPlan;
	private String nombre;
	private String alcance;
	private CentroSalud centro;
	private Departamento departamento;
	private Municipio municipio;
	private Vacuna vacuna;
	private String poblacion;
	private Date fechaInicio;
	private Date fechaFin;
	private int duracion;
	private Double presupuesto;
	private String esquema;
	private String personas;
	private Double litros;
	private int jeringa;
	
	
	
	
	public Plan() {
		
	}



	
	public Plan(int idPlan, String nombre, String alcance, CentroSalud centro, Departamento departamento, Municipio municipio, Vacuna vacuna,
			String poblacion, Date fechaInicio, Date fechaFin, int duracion, Double presupuesto, String esquema,
			String personas, Double litros, int jeringa) {
		super();
		this.idPlan = idPlan;
		this.nombre = nombre;
		this.alcance = alcance;
		this.centro = centro;
		this.departamento = departamento;
		this.municipio=municipio;
		this.vacuna = vacuna;
		this.poblacion = poblacion;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.duracion = duracion;
		this.presupuesto = presupuesto;
		this.esquema = esquema;
		this.personas = personas;
		this.litros = litros;
		this.jeringa=jeringa;
	}



	




	@Transient
	private int idCentro;
	@Transient
	public int getIdCentro() {
		return idCentro;
	}
	@Transient
	public void setIdCentro(int idCentro) {
		this.idCentro = idCentro;
	}
	
	@Transient
	private int idDepartamento;
	@Transient
	public int getIdDepartamento() {
		return idDepartamento;
	}
	@Transient
	public void setIdDepartamento(int idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	@Transient
	private int idMunicipio;
	@Transient
	public int getIdMunicipio() {
		return idMunicipio;
	}
	@Transient
	public void setIdMunicipio(int idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	
	@Transient
	private int idVacuna;
	@Transient
	public int getIdVacuna() {
		return idVacuna;
	}
	@Transient
	public void setIdVacuna(int idVacuna) {
		this.idVacuna = idVacuna;
	}
	
	

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idDepartamento")
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idMunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}




	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idVacuna")
	public Vacuna getVacuna() {
		return vacuna;
	}

	public void setVacuna(Vacuna vacuna) {
		this.vacuna = vacuna;
	}



	



	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plan_id_plan__seq")
	@SequenceGenerator(name = "plan_id_plan__seq", sequenceName = "plan_id_plan__seq", allocationSize = 1)
	@Column(name = "idPlan", unique = true, nullable = false)
	public int getIdPlan() {
		return idPlan;
	}


	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

 
	
	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "alcance", nullable = false, length = 256)
	public String getAlcance() {
		return alcance;
	}


	public void setAlcance(String alcance) {
		this.alcance = alcance;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCentro")
	public CentroSalud getCentro() {
		return centro;
	}


	public void setCentro(CentroSalud centro) {
		this.centro = centro;
	}

	@Column(name = "fechaInicio", nullable = false)
	public Date getFechaInicio() {
		return fechaInicio;
	}


	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Column(name = "fechaFin", nullable = false)
	public Date getFechaFin() {
		return fechaFin;
	}


	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Column(name = "duracion", nullable = false)
	public int getDuracion() {
		return duracion;
	}


	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	@Column(name = "presupuesto", nullable = false)
	public Double getPresupuesto() {
		return presupuesto;
	}


	public void setPresupuesto(Double presupuesto) {
		this.presupuesto = presupuesto;
	}



	@Column(name = "esquema", nullable = false, length = 100)
	public String getEsquema() {
		return esquema;
	}


	public void setEsquema(String esquema) {
		this.esquema = esquema;
	}

	
	
	@Column(name = "poblacion", nullable = false)
	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}


 
	@Column(name = "personas", nullable = false)
	public String getPersonas() {
		return personas;
	}

	public void setPersonas(String personas) {
		this.personas = personas;
	}



	@Column(name = "litros", nullable = false)
	public Double getLitros() {
		return litros;
	}




	public void setLitros(Double litros) {
		this.litros = litros;
	}

	
	@Column(name = "jeringa", nullable = false)
	public int getJeringa() {
		return jeringa;
	}

	public void setJeringa(int jeringa) {
		this.jeringa = jeringa;
	}
	
	

}
