package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "centroSalud", catalog = "sgr")

public class CentroSalud implements java.io.Serializable{
	
  private int idCentro;

  private String nombre;
  private Municipio municipio;
  
  
public CentroSalud() {
	
}

public CentroSalud(int idCentro, String nombre, Municipio municipio ) {
	
	this.idCentro = idCentro;
	
	this.nombre = nombre;
	this.municipio=municipio;
	
}
  
@Transient
private int idMunicipio;
@Transient
public int getIdMunicipio() {
	return idMunicipio;
}
@Transient
public void setIdMunicipio(int idMunicipio) {
	this.idMunicipio = idMunicipio;
}


@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "centro_id_centro__seq")
@SequenceGenerator(name = "centro_id_centro__seq", sequenceName = "centro_id_centro__seq", allocationSize = 1)
@Column(name = "idCentro", unique = true, nullable = false)
public int getIdCentro() {
	return idCentro;
}

public void setIdCentro(int idCentro) {
	this.idCentro = idCentro;
}




@Column(name = "nombre", nullable = false, length = 100)
public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}




@ManyToOne(fetch = FetchType.EAGER)
@JoinColumn(name = "idMunicipio")
public Municipio getMunicipio() {
	return municipio;
}

public void setMunicipio(Municipio municipio) {
	this.municipio = municipio;
}

  
  
}


