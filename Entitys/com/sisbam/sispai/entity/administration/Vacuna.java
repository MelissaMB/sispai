package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "vacuna", catalog = "sgr")


public class Vacuna implements java.io.Serializable{
 private int idVacuna;
 private String nombre;
 private int edad;
 private String dosis;
 private String via;
 private String enfermedad;
 private double proporcion;
 
 public Vacuna() {
		
 }

public Vacuna(int idVacuna, String nombre, int edad, String dosis, String via, String enfermedad, double proporcion) {

	this.idVacuna = idVacuna;
	this.nombre = nombre;
	this.edad = edad;
	this.dosis = dosis;
	this.via = via;
	this.enfermedad = enfermedad;
	this.proporcion = proporcion;
}
 
@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vacuna_id_vacuna_seq")
@SequenceGenerator(name = "vacuna_id_vacuna_seq", sequenceName = "vacuna_id_vacuna_seq", allocationSize = 1)
@Column(name = "idVacuna", unique = true, nullable = false)
public int getIdVacuna() {
	return idVacuna;
}

public void setIdVacuna(int idVacuna) {
	this.idVacuna = idVacuna;
}
 
@Column(name = "nombre", nullable = false, length = 100)
public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}


@Column(name = "edad", nullable = false)
public int  getEdad() {
	return edad;
}

public void setEdad(int edad) {
	this.edad = edad;
}

@Column(name = "dosis", nullable = false, length = 100)
public String getDosis() {
	return dosis;
}

public void setDosis(String dosis) {
	this.dosis = dosis;
}


@Column(name = "via", nullable = false, length = 100)
public String getVia() {
	return via;
}

public void setVia(String via) {
	this.via = via;
}

@Column(name = "enfermedad", nullable = false, length = 250)
public String getEnfermedad() {
	return enfermedad;
}

public void setEnfermedad(String enfermedad) {
	this.enfermedad = enfermedad;
}

@Column(name = "proporcion", nullable = false)
public double getProporcion() {
	return proporcion;
}

public void setProporcion(double proporcion) {
	this.proporcion = proporcion;
}

}


