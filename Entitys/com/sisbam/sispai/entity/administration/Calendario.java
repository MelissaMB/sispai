package com.sisbam.sispai.entity.administration;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "calendario", catalog = "sgp")
public class Calendario implements Serializable {
	  public int id;
	    public String title;
	    public String start;
	    public String end;
	    public String color;
	    
	    public Calendario() {
			
		}
	    
	    
	   
	    
		public Calendario(int id, String title, String start, String end, String color) {
	
			this.id = id;
			this.title = title;
			this.start = start;
			this.end = end;
			this.color = color;
		}




		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calendario_id_calendario_seq")
		@SequenceGenerator(name = "calendario_id_calendario_seq", sequenceName = "calendario_id_calendario_seq", allocationSize = 1)
		@Column(name = "idCalendario", unique = true, nullable = false)
	    public int getId() {
	        return id;
	    }
	    public void setId(int id) {
	        this.id = id;
	    }
	    
	    

		@Column(name = "title", nullable = false, length = 100)
	    public String getTitle() {
	        return title;
	    }
	  
		
		
		public void setTitle(String title) {
	        this.title = title;
	    }
	    

		@Column(name = "start", nullable = true)
	    public String getStart() {
	        return start;
	    }
	    public void setStart(String start) {
	        this.start = start;
	    }
	    

		@Column(name = "end", nullable = true)
	    public String getEnd() {
	        return end;
	    }
	    public void setEnd(String end) {
	        this.end = end;
	    }
	    @Column(name = "color", nullable = false, length = 100)
	    public String getColor() {
	        return color;
	    }
	    public void setColor(String color) {
	        this.color = color;
	    }
	 
	}