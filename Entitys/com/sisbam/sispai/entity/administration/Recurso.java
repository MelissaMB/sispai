package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "recurso", catalog = "sgr")

public class Recurso implements java.io.Serializable{

	private int idRecurso;
	private String nombre;
	private Double costo;
	private Actividad actividad;
	
	public Recurso() {
		
	}

	public Recurso(int idRecurso, String nombre, Double costo, Actividad actividad) {
		
		this.idRecurso = idRecurso;
		this.nombre = nombre;
		this.costo = costo;
		this.actividad = actividad;
	}
	
	
	@Transient
	private int idActividad;
	@Transient
	public int getIdActividad() {
		return idActividad;
	}
	@Transient
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recurso_id_recurso__seq")
	@SequenceGenerator(name = "recurso_id_recurso__seq", sequenceName = "recurso_id_recurso__seq", allocationSize = 1)
	@Column(name = "idRecurso", unique = true, nullable = false)
	public int getIdRecurso() {
		return idRecurso;
	}

	public void setIdRecurso(int idRecurso) {
		this.idRecurso = idRecurso;
	}
	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "costo", nullable = true)
	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idActividad")
	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	
	
	
}
