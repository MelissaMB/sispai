package com.sisbam.sispai.entity.administration;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


	
	@Entity
	@Table(name = "jornada", catalog = "sispai")
	public class Jornada implements java.io.Serializable {

		
		private int idJornada;
		private String nombre;
		private Date fechaInicio;
		private Date fechaFin;
	    private int duracion;
	    private Plan plan;
		private Responsable responsable;
		private boolean paralela;
		private int personas;
		private Double litros;
		private int jeringas;
		private boolean estado=true;
		
		
		
		public Jornada() {
		
		}



		public Jornada(int idJornada, String nombre, Date fechaInicio, int duracion, Date fechaFin, Plan plan,
				Responsable responsable, boolean paralela, int personas, Double litros, boolean estado, int jeringas) {
		
			this.idJornada = idJornada;
			this.nombre = nombre;
			this.fechaInicio = fechaInicio;
			this.fechaFin = fechaFin;
		    this.duracion=duracion;
			this.plan = plan;
			this.responsable = responsable;
			this.paralela = paralela;
			this.personas = personas;
			this.litros = litros;
			this.estado=estado;
			this.jeringas=jeringas;
		}







		@Transient
		private int idResponsabe;
		@Transient
		public int getIdResponsable() {
			return idResponsabe;
		}
		@Transient
		public void setIdResponsable(int idResponsabe) {
			this.idResponsabe = idResponsabe;
		}
		
		
		@Transient
		private int idPlan;
		@Transient
		public int getIdPlan() {
			return idPlan;
		}
		@Transient
		public void setIdPlan(int idPlan) {
			this.idPlan = idPlan;
		}
		
		
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jornada_id_jornada_seq")
		@SequenceGenerator(name = "jornada_id_jornada_seq", sequenceName = "jornada_id_jornada_seq", allocationSize = 1)
		@Column(name = "idJornada", unique = true, nullable = false)
		public int getIdJornada() {
			return idJornada;
		}
		public void setIdJornada(int idJornada) {
			this.idJornada = idJornada;
		}
		

		@Column(name = "nombre", nullable = false, length = 100)
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		

		
		
		
		@Column(name = "fechaInicio", nullable = true)
		public Date getFechaInicio() {
			return fechaInicio;
		}

		public void setFechaInicio(Date fechaInicio) {
			this.fechaInicio = fechaInicio;
		}
		
		@Column(name = "fechaFin", nullable = true)
		public Date getFechaFin() {
			return fechaFin;
		}

		public void setFechaFin(Date fechaFin) {
			this.fechaFin = fechaFin;
		}
		

	
		
		@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name = "idPlan")
		public Plan getPlan() {
			return plan;
		}

		public void setPlan(Plan plan) {
			this.plan = plan;
		}
		
		
		@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name = "idResponsable")
		public Responsable getResponsable() {
			return responsable;
		}

		public void setResponsable(Responsable responsable) {
			this.responsable = responsable;
		}
		


		@Column(name = "paralela", nullable = false)
		public boolean isParalela() {
			return paralela;
		}

		public void setParalela(boolean paralela) {
			this.paralela = paralela;
		}



		@Column(name = "personas", nullable = false)
		public int getPersonas() {
			return personas;
		}

		public void setPersonas(int personas) {
			this.personas = personas;
		}



		@Column(name = "litros", nullable = false)
		public Double getLitros() {
			return litros;
		}


		public void setLitros(Double litros) {
			this.litros = litros;
		}



		@Column(name = "duracion", nullable = false)
		public int getDuracion() {
			return duracion;
		}



		public void setDuracion(int duracion) {
			this.duracion = duracion;
		}


		@Column(name = "estado", nullable = false)
		public boolean isEstado() {
			return estado;
		}


		public void setEstado(boolean estado) {
			this.estado = estado;
		}



		@Column(name = "jeringas", nullable = false)
		public int getJeringas() {
			return jeringas;
		}



		public void setJeringas(int jeringas) {
			this.jeringas = jeringas;
		}

		
			
	
}
