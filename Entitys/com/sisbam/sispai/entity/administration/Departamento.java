package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "departamento", catalog = "sgr")

public class Departamento implements java.io.Serializable{
	private int idDepartamento;
	private String nombre;
	private int poblacion;

	
	public Departamento() {
	
	}

	public Departamento(int idDepartamento, String nombre, int poblacion) {
		
		this.idDepartamento = idDepartamento;
		this.nombre = nombre;
		this.poblacion = poblacion;

	}
	
	

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "depto_id_depto_seq")
	@SequenceGenerator(name = "depto_id_depto_seq", sequenceName = "depto_id_depto_seq", allocationSize = 1)
	@Column(name = "idDepartamento", unique = true, nullable = false)
	public int getidDepartamento() {
		return idDepartamento;
	}

	public void setidDepartamento(int idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "poblacion", nullable = false)
	public int getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}
	
	

}
