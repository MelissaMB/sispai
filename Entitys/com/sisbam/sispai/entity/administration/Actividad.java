package com.sisbam.sispai.entity.administration;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;



@Entity
@Table(name = "actividad", catalog = "sgp")
public class Actividad implements java.io.Serializable {

	
	private int idActividad;
	private String nombre;
	private Date fechaInicio;
	private Date fechaFin;
	private int duracion;
    private Plan plan;
	private Responsable responsable;
	private boolean paralela;
	private int personas;
	private Double litros;
	
	
	
	public Actividad() {


	}
	


	
	public Actividad(int idActividad, String nombre, Date fechaInicio, Date fechaFin, int duracion, Plan plan,
			Responsable responsable, boolean paralela, int personas, Double litros) {
		
		this.idActividad = idActividad;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.duracion = duracion;
		this.plan = plan;
		this.responsable = responsable;
		this.paralela = paralela;
		this.personas = personas;
		this.litros = litros;
	}




	@Transient
	private int idResponsabe;
	@Transient
	public int getIdResponsable() {
		return idResponsabe;
	}
	@Transient
	public void setIdResponsable(int idResponsabe) {
		this.idResponsabe = idResponsabe;
	}
	
	
	@Transient
	private int idPlan;
	@Transient
	public int getIdPlan() {
		return idPlan;
	}
	@Transient
	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "actividad_id_actividad_seq")
	@SequenceGenerator(name = "actividad_id_actividad_seq", sequenceName = "actividad_id_actividad_seq", allocationSize = 1)
	@Column(name = "idActividad", unique = true, nullable = false)
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	

	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	
	
	
	@Column(name = "fechaInicio", nullable = true)
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	@Column(name = "fechaFin", nullable = true)
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	

	@Column(name = "duracion", nullable = false)
	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPlan")
	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idResponsable")
	public Responsable getResponsable() {
		return responsable;
	}

	public void setResponsable(Responsable responsable) {
		this.responsable = responsable;
	}
	


	@Column(name = "paralela", nullable = false)
	public boolean isParalela() {
		return paralela;
	}

	public void setParalela(boolean paralela) {
		this.paralela = paralela;
	}



	@Column(name = "personas", nullable = false)
	public int getPersonas() {
		return personas;
	}

	public void setPersonas(int personas) {
		this.personas = personas;
	}



	@Column(name = "litros", nullable = false)
	public Double getLitros() {
		return litros;
	}


	public void setLitros(Double litros) {
		this.litros = litros;
	}



	
	
}
