package com.sisbam.sispai.entity.administration;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "municipio", catalog = "sgr")
public class Municipio implements java.io.Serializable{
	
	private int idMunicipio;
	private String nombre;
	private int poblacion;
	private Departamento departamento;
	public Municipio() {


	}

	public Municipio(int idMunicipio, String nombre, int poblacion, Departamento departamento) {


		this.idMunicipio = idMunicipio;
		this.nombre = nombre;
		this.poblacion = poblacion;
		this.departamento=departamento;
	}
	

	@Transient
	private int idDepartamento;
	@Transient
	public int getIdDepartamento() {
		return idDepartamento;
	}
	@Transient
	public void setIdDepartamento(int idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "municipio_id_depto_seq")
	@SequenceGenerator(name = "municipio_id_depto_seq", sequenceName = "municipio_id_depto_seq", allocationSize = 1)
	@Column(name = "idMunicipio", unique = true, nullable = false)
	public int getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(int idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	
	
	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "poblacion", nullable = false)
	public int getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idDepartamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

}
