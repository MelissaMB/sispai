package com.sisbam.sispai.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "estrategia", catalog = "sgp")
public class Estrategia implements java.io.Serializable  {
	
	private int idEstrategia;
	private String nombre;
	private String descripcion;
	private String alcance;
	
	
	public Estrategia() {
		
	}


	public Estrategia(int idEstrategia, String nombre, String descripcion, String alcance) {
		
		this.idEstrategia = idEstrategia;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.alcance = alcance;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estrategia_idestrategia_seq")
	@SequenceGenerator(name = "estrategia_idestrategia_seq", sequenceName = "estrategia_idestrategia_seq", allocationSize = 1)
	@Column(name = "idEstrategia", unique = true, nullable = false)
	public int getIdEstrategia() {
		return idEstrategia;
	}


	public void setIdEstrategia(int idEstrategia) {
		this.idEstrategia = idEstrategia;
	}

	@Column(name = "nombre", nullable = false, length = 256)
	public String getnombre() {
		return nombre;
	}


	public void setnombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "descripcion", nullable = false, length = 256)
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "alcance", nullable = false, length = 256)
	public String getAlcance() {
		return alcance;
	}


	public void setAlcance(String alcance) {
		this.alcance = alcance;
	}
	
	
	

}
