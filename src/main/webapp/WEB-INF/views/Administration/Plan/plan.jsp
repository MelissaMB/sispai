<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div id="contenido" class="card-panel hoverable">

<c:if test="${createplanS}">	
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="/sispai/plan/add/"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

		
		<hr>	
	 	<div class="container">	
<c:if test="${readplanS}">	
			<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Esquema</th>
						<th>Poblaci�n objetivo</th>
				     	<th>Municipio</th>
						<th>Duraci�n <br>[d�as]</br></th>
						<th>Opci�n</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${planes}" var="plan">
						<tr style="color:#0B0B61;">
							<td>${plan.nombre }</td>
							 <td>${plan.esquema}</td>
							 <td>${plan.poblacion}</td>
							 <td>${plan.municipio.nombre}</td>
							 <td>${plan.duracion }</td>
		
							<td width="5%">
							<c:if test="${updateplanS}">	
									<a class="modal-trigger tooltipped" href="#-${plan.idPlan}" data-position="left" data-tooltip="Actualizar"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;

							</c:if>
							
		                   
							<a class="tooltipped" href="/sispai/jornadas/delete/${plan.idPlan}" data-toggle="modal"data-target="#" onclick="return confirmDel('${plan.idPlan}');" data-position="bottom" data-tooltip="Eliminar"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;
							<a class="tooltipped" href="/sispai/plan-jornada/${plan.idPlan}" data-position="right" data-tooltip="Jornadas de Vacunaci�n"><i class="	fa fa-calendar green-text"></i></a>
							<c:if test="${deleteplanS}">		
									
							</c:if>						
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>		
	</div>
</div>



<!-- /.col-lg-12 -->


<c:forEach items="${planes}" var="plan">
<div id="-${plan.idPlan}" class="modal white darken-4">
<div class="modal-header"></div>
	<div class="modal-content">
	
	<form:form method="post" modelAttribute="planForm"
			action="/sispai/planes/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
           
         
			<div class="row">  
			 <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Datos Generales</h5></legend>
				
				
			<div class="row">
				<div class="input-field col s12">
					<form:input path="nombre" class="validate"
						type="text" id="nombre" value="${plan.nombre }" required="required"/>
						<label for="nombre">Nombre </label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="alcance" class="validate"
						type="text" id="alcance" value="${plan.alcance }" required="required"/>
						<label for="alcance">Alcance </label>
				</div>
			</div>
				
				<div class="input-field col s6">
			<form:select path="idMunicipio" name="idMunicipio" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Municipio</option>
						<c:forEach items="${municipios}" var="id">
							<c:choose>
								<c:when test="${plan.municipio.idMunicipio == id.idMunicipio}">
									<form:option value="${id.idMunicipio}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idMunicipio}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>	
				
				<div class="input-field col s6">
			<form:select path="idCentro" name="idCentro" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Centro de salud</option>
						<c:forEach items="${centros}" var="id">
							<c:choose>
								<c:when test="${plan.centro.idCentro == id.idCentro}">
									<form:option value="${id.idCentro}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idCentro}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>	
					
					
				<div class="input-field col s6">
					<form:select path="esquema" id="esquema" class="form-control" value="${plan.esquema }" required="required">
				<option value="" disabled selected>Elige un esquema de Plan de acci�n</option>
				<option value="B�sico">Donacion</option>
				<option value="Refuerzo">Refuerzo</option>
				<option value="Alto riesgo">Alto riesgo</option>
				<option value="Mixto">Mixto</option>
				</form:select>
				</div>	
				
				<div class="input-field col s6">
					<form:select path="poblacion" id="poblacion" class="form-control" value="${plan.poblacion }" required="required">
				<option value="" disabled selected>Elige un esquema de Plan de acci�n</option>
				<option value="Recien nacido">Recien nacido</option>
				<option value="2 meses">2 meses</option>
				<option value="4 meses">4 meses</option>
				<option value="6 meses">6 meses</option>
				</form:select>
				</div>									
												
				
				</fieldset>
				
				<fieldset style="border-radius:15px;">
				<legend><h5 style="text-align: center; "> Tiempo y Presupuesto</h5></legend>
			<div class="date">
			 
				<div class="input-field col s6">
				 
					<form:input path="fechaInicio" class="form-control" placeholder="fecha"
						type="date" id="fechaInicio" value="${plan.fechaInicio}" required="required"/>
				</div>
			
			</div>
			
			<div class="date">
				<div class="input-field col s6">
				
					<form:input path="fechaFin" class="form-control" placeholder="fecha"
						type="date" id="fechaFin" value="${plan.fechaFin}" required="required"/>
				</div>
			</div>
			
			
			<input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" value="Calcular Duracion del Plan de Acci�n" onclick="calcularDias();">
			
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="duracion" class="form-control" placeholder="En dias"
						type="text"   value="${plan.duracion}" required="required" id="resultado"/>
			</div>
			</div>
			
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="presupuesto" class="form-control" placeholder="Presupuesto estimado : $"
						type="price"   value="${plan.presupuesto}" required="required" id="presupuesto"/>
			</div>
			</div>
				
			</fieldset>
			
			<form:hidden path="idPlan" value="${plan.idPlan}" />
			
	
			

		</fieldset>
		
		
		
		<fieldset style="border-radius:15px;">
				<legend><h5 style="text-align: center; "> Recursos Requeridos</h5></legend>
				
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="personas" class="form-control" placeholder="Personas requeridas"
						type="number"   value="${plan.personas}" required="required" id="personas"/>
			<label for="personas">Personas requeridas </label>
			</div>
			</div>
			
			<div class="input-field col s6">
			<form:select path="idVacuna" name="idVacuna" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Vacuna</option>
						<c:forEach items="${vacunas}" var="id">
							<c:choose>
								<c:when test="${plan.vacuna.idVacuna == id.idVacuna}">
									<form:option value="${id.idVacuna}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idVacuna}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>	
				
			<div class="row">	
			<div  class="input-field col s6">
			<form:input path="litros" class="form-control" placeholder="Litros de vacuna"
						type="number"   value="${plan.litros}" required="required" id="litros"/>
			<label for="vacunas">Litros de Vacuna </label>
			</div>
			</div>
				
		</fieldset>
		

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
		
	</div>
</div>


</c:forEach>







<script  type="text/javascript">
function calcularDias()
{
	var fechaInicial=document.getElementById("fechaInicio").value;
	var fechaFinal=document.getElementById("fechaFin").value;
	var resultado="";
	
	
	inicial=fechaInicial.split("-");
	finals=fechaFinal.split("-");
	// obtenemos las fechas en milisegundos
	var dateStart=new Date(inicial[0],(inicial[1]-1),inicial[2]);
	var dateEnd=new Date(finals[0],(finals[1]-1),finals[2]);
        if(dateStart<dateEnd)
        {
			// la diferencia entre las dos fechas, la dividimos entre 86400 segundos
			// que tiene un dia, y posteriormente entre 1000 ya que estamos
			// trabajando con milisegundos.
			resultado=(((dateEnd-dateStart)/86400)/1000);
		}else{
			resultado="La fecha inicial es posterior a la fecha final";
		}
        
	
	document.getElementById("resultado").value=resultado;
}

</script>


<script>

function Borrar(codProyecto)
{

 var resul = confirm('�Desea borrar el proyecto seleccionado?');
 if(resul=true)
	 {
	   location.href="/sgp/proyectos/delete/"+codProyecto;
	 }
 else (resul=false)
 {
	 location.href="/sgp/proyectos";
	}
 
} 

</script>

<script>
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
  function confirmDel(){
	  var agree=confirm("�Realmente desea eliminarlo? ");
	  if (agree) return true ;
	  return false;
	} 
  
</script>





