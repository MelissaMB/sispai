<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="contenido" class="card-panel hoverable">

<hr>
	<div class="container">
	       <h3 style="text-align:center;">${plan.nombre}</h3>
			<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Duraci�n</th>
						<th>Estado</th>
						<th>Fecha Inicio</th>
						<th>Opci�n</th>
						
													
						
					</tr>
				</thead>
				
				<tbody>
					<c:forEach items="${jornadas}" var="jornada">
						<tr style="color:#0B0B61;">
							<td>${jornada.nombre }</td>
				
							 	<td>${jornada.duracion}</td>
							 	<td><div class="progress">
      						<div class="determinate" style="width:${jornada.estado}%"></div>
      						</div>
  							</td>
  							<td><fmt:formatDate value="${jornada.fechaInicio }" pattern="dd-MM-YYYY"/></td>
  							<td><a class="tooltipped" href="/sispai/jornadas/calendar/${plan.idPlan}" data-position="right" data-tooltip="Calendario de Jornadas"><i class="	fa fa-calendar green-text"></i></a></td>
						</tr>
					</c:forEach>
							
				</tbody>
				
				
			</table>
			
			
				
			
				

	</div>
</div>