<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Graphical View</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic()
{
var data = new google.visualization.DataTable();
data.addColumn('string', 'Planes de Acci�n');
data.addColumn('number', 'Cantidad de personas');

<c:forEach items="${planes}" var="listplayers">
data.addRow(['${listplayers.nombre}',${listplayers.personas}]);
</c:forEach>

  var options = {
    title: 'Cantidad de Personas estimadas por cada Plan de Acci�n',
    colors: ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
    hAxis: {
      title: 'Planes de Acci�n'
    },
    vAxis: {
      title: 'Cantidad de personas'
    }
  };

  var chart = new google.visualization.ColumnChart(
    document.getElementById('chart_div'));

  chart.draw(data, options);
}



</script>
</head>
<body>
<div id="chart_div"></div>
</body>
</html>             