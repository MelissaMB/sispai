<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="contenido" class="card-panel hoverable">

<hr>
	<div class="container">
	     
			<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Departamento</th>
						<th>Poblaci�n</th>
						<th>Opci�n</th>
						
													
						
					</tr>
				</thead>
				
				<tbody>
					<c:forEach items="${departamentos}" var="departamento">
						<tr style="color:#0B0B61;">
							    <td>${departamento.nombre }</td>
							 	<td>${departamento.poblacion}</td>
  						 
  							<td><a class="tooltipped" href="/sispai/poblaciones/${departamento.idDepartamento}" data-position="right" data-tooltip="Poblaci�n por Municipios"><i class="fa fa-users "></i></a></td>
						</tr>
					</c:forEach>
							
				</tbody>
				
				
			</table>
			
			
				
			
				

	</div>
</div>