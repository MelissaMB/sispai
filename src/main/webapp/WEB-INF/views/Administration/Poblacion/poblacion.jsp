<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

    
<div id="contenido" class="card-panel hoverable">
         

	 	<div class="container">
		<c:if test="${readpoblacionS}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Municipio</th>
					<th>Poblaci�n</th>
			
					
	
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${municipios}" var="municipio">
					<tr>
						<td>${municipio.nombre }</td>
						<td><fmt:setLocale value="en_US" /><fmt:formatNumber value="${municipio.poblacion }" pattern="###, ###, ###"/> </td>
					
						
						<td width="5%">
							<c:if test="${updateactividadS}">	
						    <a class="modal-trigger tooltipped" href="#-${municipio.idMunicipio}" data-position="left" data-tooltip="Actualizar"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
												
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Actvdades</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="municipioForm"
			action="/sispai/poblaciones/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
               
             
              <fieldset style="border-radius:15px;">
			
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${municipio.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			
			 
		
			<form:hidden id="idMunicipio" path="idMunicipio" value="${municipio.idMunicipio}" />
		
			
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="poblacion" class="form-control" placeholder="Poblacion estimada"
						type="number"   value="${municipio.poblacion}" required="required" id="poblacion"/>
			<label for="personas">Poblacion estimada </label>
			</div>
			</div>
			
			
				
		</fieldset>
            	
			</fieldset>
            <br>
            <br>
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>


<c:forEach items="${municipios}" var="municipio">
<div id="-${municipio.idMunicipio}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

 <form:form method="post" modelAttribute="municipioForm"
			action="/sispai/poblaciones/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
               
             
              <fieldset style="border-radius:15px;">
			
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${municipio.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			
			 
		
			<form:hidden id="idMunicipio" path="idMunicipio" value="${municipio.idMunicipio}" />
		
			
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="poblacion" class="form-control" placeholder="Poblacion estimada"
						type="number"   value="${municipio.poblacion}" required="required" id="poblacion"/>
			<label for="personas">Poblacion estimada </label>
			</div>
			</div>
			
			
				
		</fieldset>
            	
			</fieldset>
            <br>
            <br>
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>





<script  type="text/javascript">
function calcularDias()
{
	var fechaInicial=document.getElementById("fechaInicio").value;
	var fechaFinal=document.getElementById("fechaFin").value;
	var resultado="";
	
	
	inicial=fechaInicial.split("-");
	finals=fechaFinal.split("-");
	// obtenemos las fechas en milisegundos
	var dateStart=new Date(inicial[0],(inicial[1]-1),inicial[2]);
	var dateEnd=new Date(finals[0],(finals[1]-1),finals[2]);
        if(dateStart<dateEnd)
        {
			// la diferencia entre las dos fechas, la dividimos entre 86400 segundos
			// que tiene un dia, y posteriormente entre 1000 ya que estamos
			// trabajando con milisegundos.
			resultado=(((dateEnd-dateStart)/86400)/1000);
		}else{
			resultado="La fecha inicial es posterior a la fecha final";
		}
        
	
	document.getElementById("resultado").value=resultado;
}

</script>


<script>

function Borrar(idActividad)
{

 var resul = confirm('�Desea borrar la Actividad seleccionada?');
 if(resul=true)
	 {
	   location.href="/sispai/actividades/delete/"+idActividad;
	 }
 else (resul=false)
 {
	 location.href="/sispai/actividades";
	}
 
} 

</script>



    