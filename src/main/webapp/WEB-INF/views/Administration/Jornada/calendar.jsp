<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

   

<link href="<c:url value="${context }/resources/vendor/fullcalendar/css/fullcalendar.min.css"/>" rel="stylesheet" type="text/css" />


  <script src="<c:url value="${context }/resources/vendor/fullcalendar/js/jquery.min.js" />"></script>
 <script src="<c:url value="${context }/resources/vendor/fullcalendar/js/moment.min.js" />"></script>
  <script src="<c:url value="${context }/resources/vendor/fullcalendar/js/fullcalendar.min.js" />"></script>
 </head>
 



      

<script> $(document).ready(function() 
{ $('#calendar').fullCalendar({ defaultDate: '2019-01-01',
 editable: true, 
 eventLimit: true,// allow "more" link when too many events 
 events: 'http://localhost:8081/sispai/jornadas/calendar/{id}'
	 
 }); });
 </script> 



    <h1 style=" text-align: center;">Calendario de Jornadas de Vacunación</h1>
    <h2  style=" text-align: center;">Plan</h2>
 
   
    <body style="margin: 40px 10px; padding: 0; font-family: Lucida Grande,Helvetica,Arial,Verdana,sans-serif; font-size: 14px;">
        <div id="calendar" style="max-width: 900px; margin: 0 auto;"></div>
    </body>
</html>




   