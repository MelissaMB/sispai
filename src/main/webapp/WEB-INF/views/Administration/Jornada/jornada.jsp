<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

    
<div id="contenido" class="card-panel hoverable">
<c:if test="${createjornadaS}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

<hr>
	 	<div class="container">
		<c:if test="${readjornadaS}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Jornada</th>
					
					<th>Responsable</th>
					<th>Plan</th>
					
					
	
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${jornadas}" var="jornada">
					<tr>
						<td>${jornada.nombre }</td>
						
						<td>${jornada.responsable.nombre }</td>
						<td>${jornada.plan.nombre}</td>
						
						
						<td width="5%">
							<c:if test="${updatejornadaS}">	
									<a class=" modal-trigger" href="#-${jornada.idJornada }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deletejornadaS}">		
									<a class="" href="/sispai/jornadas/delete/${jornada.idJornada }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Actvdades</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="jornadaForm"
			action="/sispai/jornadas/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
               
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Jornada de Vacunaci�n</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${jornada.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 
			<form:hidden id="idJornada" path="idJornada" value="${jornada.idJornada}" />
			 
			 
			 	   <div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idPlan" name="idPlan" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Plan de Acci�n</option>
						<c:forEach items="${planes}" var="id">
							<c:choose>
								<c:when test="${jornada.plan.idPlan == id.idPlan}">

									<form:option value="${id.idPlan}" label="${id.nombre}"
										selected="true" />
										
								</c:when>
								<c:otherwise>
									<form:option value="${id.idPlan}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
		 <a class="" href="javascript:getURL()"  >  <input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" value=" Calendario de Jornadas de Vacunaci�n"> </a>
			
	<br>
			 <div class="date">
			 
				<div class="input-field col s6">
				   <label for="inicio">Fecha inicio: </label>
				   <br>
					<form:input path="fechaInicio" class="form-control" placeholder="fecha"
						type="date" id="fechaInicio" value="${jornada.fechaInicio}" required="required"/>
				</div>
			
			</div>
			
			
			<div class="date">
				<div class="input-field col s6">
				    <label for="fin">Fecha fin: </label>
				    <br>
					<form:input path="fechaFin" class="form-control" placeholder="fecha"
						type="date" id="fechaFin" value="${jornada.fechaFin}" required="required"/>
				</div>
			</div>
			
	

	
	<input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" value="Estimar duraci�n de Jornada" onclick="calcularDias();"> <a class="" href="/sispai/jornadas/calendar"  ></a>
			<div class="row">
				
			      <div  class="input-field col s12">
			      <form:input path="duracion" class="form-control" placeholder="Duracion"
						type="text"   value="${jornada.duracion}" required="required" id="resultado"/>
				 </div>
			</div>
	
			<div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idResponsable" name="idResponsable" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Responsable</option>
						<c:forEach items="${responsables}" var="id">
							<c:choose>
								<c:when test="${jornada.responsable.idResponsable == id.idResponsable}">
									<form:option value="${id.idResponsable}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idResponsable}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
			
	
			
	   <label for="paralela">Estado de la Actividad </label>
   
       	<div class="input-field col s12">
					<form:select path="paralela" id="paralela" class="form-control" value="${jornada.paralela }" required="required">
				<option value="" disabled selected>Seleccionar un estado</option>
				<option value="true">Paralela </option>
				<option value="false">No paralela  </option>
			
			</form:select>
			</div>									
							
      
   <br>
			<form:hidden id="idJornada" path="idJornada" value="${jornada.idJornada}" />
			<form:hidden id="estado" path="estado" value="${jornada.estado}" />
		
			<fieldset style="border-radius:15px;">
				<legend><h5 style="text-align: center; "> Recursos Requeridos</h5></legend>
				
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="personas" class="form-control" placeholder="Personas requeridas"
						type="number"   value="${jornada.personas}" required="required" id="personas"/>
			<label for="personas">Personas requeridas </label>
			</div>
			</div>
			
			
			<div class="row">	
			<div  class="input-field col s6">
			<form:input path="litros" class="form-control" placeholder="Litros de vacuna"
						type="number"   value="${jornada.litros}" required="required" id="litros"/>
			<label for="vacunas">Litros de Vacuna </label>
			</div>
			</div>
				
		</fieldset>
            	
			</fieldset>
            <br>
            <br>
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>


<c:forEach items="${jornadas}" var="jornada">
<div id="-${jornada.idJornada}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="jornadaForm"
			action="/sispai/jornadas/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		          
                     <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Jornada de Vacunaci�n</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${jornada.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			
			 
		
			 <div class="date">
			 
				<div class="input-field col s6">
				 
					<form:input path="fechaInicio" class="form-control" placeholder="fecha"
						type="date" id="fechaInicio" value="${jornada.fechaInicio}" required="required"/>
				</div>
			
			</div>
			
			
			<div class="date">
				<div class="input-field col s6">
				
					<form:input path="fechaFin" class="form-control" placeholder="fecha"
						type="date" id="fechaFin" value="${jornada.fechaFin}" required="required"/>
				</div>
			</div>
			
			 <a class="" href="javascript:getURL()"  >  <input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" onclick="valida();" value="Ver Calendario de Jornadas de Vacunaci�n"> </a>
	
	
	<input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" value="Estimar duraci�n de Jornada" onclick="calcularDias();"> <a class="" href="/sispai/jornadas/calendar"  ></a>
			<div class="row">
				
			      <div  class="input-field col s12">
			      <form:input path="duracion" class="form-control" placeholder="Duracion"
						type="text"   value="${jornada.duracion}" required="required" id="resultado"/>
				 </div>
			</div>
			
			
			<div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idResponsable" name="idResponsable" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Responsable</option>
						<c:forEach items="${responsables}" var="id">
							<c:choose>
								<c:when test="${jornada.responsable.idResponsable == id.idResponsable}">
									<form:option value="${id.idResponsable}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idResponsable}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
			
		   <div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idPlan" name="idPlan" class="form-control" required="true">
						<option value="" disabled selected>Selecione un Plan de Acci�n</option>
						<c:forEach items="${planes}" var="id">
							<c:choose>
								<c:when test="${jornada.plan.idPlan == id.idPlan}">
									<form:option value="${id.idPlan}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idPlan}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
			
	   <label for="paralela">Estado de la Actividad </label>
   
       	<div class="input-field col s12">
					<form:select path="paralela" id="paralela" class="form-control" value="${jornada.paralela }" required="required">
				<option value="" disabled selected>Seleccionar un estado</option>
				<option value="true">Paralela </option>
				<option value="false">No paralela  </option>
			
			</form:select>
			</div>									
							
      
   <br>
			<form:hidden id="idJornada" path="idJornada" value="${jornada.idJornada}" />
			<form:hidden id="estado" path="estado" value="${jornada.estado}" />
		
			<fieldset style="border-radius:15px;">
				<legend><h5 style="text-align: center; "> Recursos Requeridos</h5></legend>
				
			<div class="row">	
			<div  class="input-field col s12">
			<form:input path="personas" class="form-control" placeholder="Personas requeridas"
						type="number"   value="${jornada.personas}" required="required" id="personas"/>
			<label for="personas">Personas requeridas </label>
			</div>
			</div>
			
			<div class="row">	
			<div  class="input-field col s6">
			<form:input path="jeringas" class="form-control" placeholder="Cantidad de Jeringas"
						type="number"   value="${jornada.jeringas}" required="required" id="personas"/>
			<label for="personas">Personas requeridas </label>
			</div>
			</div>
			
			<div class="row">	
			<div  class="input-field col s6">
			<form:input path="litros" class="form-control" placeholder="Litros de vacuna"
						type="number"   value="${jornada.litros}" required="required" id="litros"/>
			<label for="vacunas">Litros de Vacuna </label>
			</div>
			</div>
				
	
			</fieldset>
            <br>
            <br>
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>





<script  type="text/javascript">
function calcularDias()
{
	var fechaInicial=document.getElementById("fechaInicio").value;
	var fechaFinal=document.getElementById("fechaFin").value;
	var resultado="";
	
	
	inicial=fechaInicial.split("-");
	finals=fechaFinal.split("-");
	// obtenemos las fechas en milisegundos
	var dateStart=new Date(inicial[0],(inicial[1]-1),inicial[2]);
	var dateEnd=new Date(finals[0],(finals[1]-1),finals[2]);
        if(dateStart<dateEnd)
        {
			// la diferencia entre las dos fechas, la dividimos entre 86400 segundos
			// que tiene un dia, y posteriormente entre 1000 ya que estamos
			// trabajando con milisegundos.
			resultado=(((dateEnd-dateStart)/86400)/1000);
		}else{
			resultado="La fecha inicial es posterior a la fecha final";
		}
        
	
	document.getElementById("resultado").value=resultado;
}

</script>


<script>

function Borrar(idActividad)
{

 var resul = confirm('�Desea borrar la Actividad seleccionada?');
 if(resul=true)
	 {
	   location.href="/sispai/actividades/delete/"+idActividad;
	 }
 else (resul=false)
 {
	 location.href="/sispai/actividades";
	}
 
} 

</script>


<!-- FUNCI�N PARA TOMAR ID DE SELECT y enviarlo a url-->
<script>
function getURL()
{
	 if (document.getElementById('idPlan').value == "")
		 {
		 alert("Seleccione un Plan de Acci�n para consultar Calendario");
		    window.location.reload(true);
		 
		 }
	 else
		 {
var dir = "/sispai/jornadas/calendar/";
dir += document.getElementById('idPlan').value;

window.location.href=dir;}
}
</script>

<!-- FUNCI�N ALERT PARA VALIDAR SELECCI�N DE PLAN Y VER CALENDARIO  -->


    