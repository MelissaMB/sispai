<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<!-- Modal Trigger -->
  

<div id="contenido" class="card-panel hoverable">
<c:if test="${createcentrosS}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>
<c:if test="${readcentrosS}">
		<a class="waves-effect waves-light btn-floating modal-trigger red darken-3" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</a>
		<a class="waves-effect waves-light btn-floating modal-trigger green darken-3" href="#"><i class="fa fa-file-excel-o" aria-hidden="true"></i> EXCEL</a>
</c:if>
<hr>
	 	<div class="container">
		<c:if test="${readcentrosS}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					
					<th>Nombre</th>
					<th>Ubicaci�n</th>
				
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${centros}" var="centro">
					<tr>
						<td>${centro.nombre}</td>
						<td>${centro.municipio.nombre}</td>
				
					
						<td width="5%">
							<c:if test="${updatecentrosS}">	
									<a class=" modal-trigger" href="#-${centro.idCentro }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deletecentrosS}">		
									<a class="" href="/sispai/centros/delete/${centro.idCentro  }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Empresa</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="centrosForm"
			action="/sispai/centros/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Centro salud</h5></legend>
			<div class="row">
		
			
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="nombre" value="${centro.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />

				</div>
			 </div>
			
				
				<div class="row">
				<div class="input-field col s6">
				
												
					<form:select path="idMunicipio" name="idMunicipio" class="form-control" required="true">
						<option value="" disabled selected>Selecione el Municipio</option>
						<c:forEach items="${municipios}" var="id">
							<c:choose>
								<c:when test="${centro.municipio.idMunicipio == id.idMunicipio}">
									<form:option value="${id.idMunicipio}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idMunicipio}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				
				</div>
			
			
		
			<form:hidden id="idCentro" path="idCentro" value="${centro.idCentro}" />

			</fieldset>

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

<c:forEach items="${centros}" var="centro">
<div id="-${centro.idCentro}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="centrosForm"
			action="/sispai/centros/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		          <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; ">Edici�n Centro salud</h5></legend>
			<div class="row">
		
			
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="nombre" value="${centro.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />

				</div>
			 </div>
			
				
				<div class="row">
				<div class="input-field col s6">
				
												
					<form:select path="idMunicipio" name="idMunicipio" class="form-control" required="true">
						<option value="" disabled selected>Selecione el Municipio</option>
						<c:forEach items="${municipios}" var="id">
							<c:choose>
								<c:when test="${centro.municipio.idMunicipio == id.idMunicipio}">
									<form:option value="${id.idMunicipio}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idMunicipio}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				
				</div>
			
			
		
			<form:hidden id="idCentro" path="idCentro" value="${centro.idCentro}" />

			</fieldset>
			
	<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>




<script>
$("#idZona").on("change", function (e) {
	$.ajax({
    url: "../centros/add",
    method: "GET",
    data: { idZon: $(this).val() },
    dataType: "json",
    success: function (response) {
      if (response) {
        $("#idDepto option:not(:disabled)").remove();
        $.each(response, function(addCentro, option) {
        	console.log("option: "+option)
        	$("#idDepto").append('<option value="'+ option.id +'">'+ option.value +'</option>') 
        });
      }
    }
  });
});
</script>

