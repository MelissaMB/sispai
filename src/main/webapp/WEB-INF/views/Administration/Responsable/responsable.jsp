<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<!-- Modal Trigger -->
  

<div id="contenido" class="card-panel hoverable">
<c:if test="${createresponsbalex23}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

<hr>
	 	<div class="container">
		<c:if test="${readresponsbalex23}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Funci�n</th>
					<th>Contacto</th>
	
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${responsables}" var="responsable">
					<tr>
						<td>${responsable.nombre }</td>
						<td>${responsable.funcion }</td>
						<td>${responsable.email }</td>
						
						<td width="5%">
							<c:if test="${updateresponsbalex23}">	
									<a class=" modal-trigger" href="#-${responsable.idResponsable }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deleteresponsbalex23}">		
									<a class="" href="/sispai/responsables/delete/${responsable.idResponsable }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Empresa</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="responsableForm"
			action="/sispai/responsables/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Responsable</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${responsable.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s12">
					<form:input path="funcion" value="${responsable.funcion }"
						placeholder="Funci�n a desempe�ar..." id="funcion" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			  <div class="input-field col s6">
					<form:input path="email" value="${responsable.email }"
						placeholder="Email" id="email" type="email" class="validate"
						 required="required" />

				</div>
			 </div>
				
			
				
				
			
			<form:hidden id="idResponsable" path="idResponsable" value="${responsable.idResponsable}" />

			</fieldset>

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>









<c:forEach items="${responsables}" var="responsable">
<div id="-${responsable.idResponsable}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="responsableForm"
			action="/sispai/responsables/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		        
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Edici�n Responsable</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${responsable.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s12">
					<form:input path="funcion" value="${responsable.funcion }"
						placeholder="Funci�n a desempe�ar..." id="funcion" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			  <div class="input-field col s6">
					<form:input path="email" value="${responsable.email }"
						placeholder="Email" id="email" type="email" class="validate"
						 required="required" />

				</div>
			 </div>
				
			
				
				
			
			<form:hidden id="idResponsable" path="idResponsable" value="${responsable.idResponsable}" />

			</fieldset>

			
			
			
			
	<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>

