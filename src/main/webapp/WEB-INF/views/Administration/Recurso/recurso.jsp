<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

    
<div id="contenido" class="card-panel hoverable">
<c:if test="${createrecursoS}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

<hr>
	 	<div class="container">
		<c:if test="${readrecursoS}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Recurso</th>
					<th>Costo</th>
					<th>Actividad</th>
					
					
					
	
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${recursos}" var="recurso">
					<tr>
						<td>${recurso.nombre }</td>
						<td>$ ${recurso.costo }</td>
						<td>${recurso.actividad.nombre }</td>
						
						
						<td width="5%">
							<c:if test="${updaterecursoS}">	
									<a class=" modal-trigger" href="#-${recurso.idRecurso }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deleterecursoS}">		
									<a class="" href="/sispai/recursos/delete/${recurso.idRecurso }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Actvdades</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="recursoForm"
			action="/sispai/recursos/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
               
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Recurso</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${recurso.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s6">
					<form:input path="costo" value="${recurso.costo }"
						placeholder="Detalle de costo $..." id="costo" type="price" class="validate"
						 />
						 
				</div>
			 </div>
			 
			 
			 
			 <div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idActividad" name="idActividad" class="form-control" required="true">
						<option value="" disabled selected>Selecione Actividad a asignar recurso</option>
						<c:forEach items="${actividades}" var="id">
							<c:choose>
								<c:when test="${recurso.actividad.idActividad == id.idActividad}">
									<form:option value="${id.idActividad}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idActividad}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
		
			
			
			<form:hidden id="idRecurso" path="idRecurso" value="${recurso.idRecurso}" />
		
			
            	
			</fieldset>
            <br>
            <br>
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>


<c:forEach items="${recursos}" var="recurso">
<div id="-${recurso.idRecurso}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="recursoForm"
			action="/sispai/recursos/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		          
                
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Edici�n de Recurso</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${recurso.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s6">
					<form:input path="costo" value="${recurso.costo }"
						placeholder="Detalle de costo $..." id="costo" type="price" class="validate"
						 />
						
				</div>
			 </div>
			 
			 
			 
			 <div class="row">
				<div class="input-field col s12">
				
												
					<form:select path="idActividad" name="idActividad" class="form-control" required="true">
						<option value="" disabled selected>Selecione Actividad a asignar recurso</option>
						<c:forEach items="${actividades}" var="id">
							<c:choose>
								<c:when test="${recurso.actividad.idActividad == id.idActividad}">
									<form:option value="${id.idActividad}" label="${id.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${id.idActividad}" label="${id.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
				</div>
			
		
			
			
			<form:hidden id="idRecurso" path="idRecurso" value="${recurso.idRecurso}" />
		
			
            	
			</fieldset>
            <br>
			
	<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>
