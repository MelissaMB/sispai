<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<!-- Modal Trigger -->
  

<div id="contenido" class="card-panel hoverable">
<c:if test="${createestrategiax23}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

<hr>
	 	<div class="container">
		<c:if test="${readestrategiax23}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Estrategia</th>
					<th>Descripción</th>
					<th>Alcance</th>
	
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${estrategias}" var="estrategia">
					<tr>
						<td>${estrategia.nombre }</td>
						<td>${estrategia.descripcion }</td>
						<td>${estrategia.alcance }</td>
						
						<td width="5%">
							<c:if test="${updateestrategiax23}">	
									<a class=" modal-trigger" href="#-${estrategia.idEstrategia }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deleteestrategiax23}">		
									<a class="" href="/sispai/estrategias/delete/${estrategia.idEstrategia }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Empresa</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="estrategiaForm"
			action="/sispai/estrategias/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro de Estrategía</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${estrategia.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s12">
					<form:input path="descripcion" value="${estrategia.descripcion }"
						placeholder="Descripción..." id="descripcion" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			  <div class="row">
			     <div class="input-field col s12">
					<form:input path="alcance" value="${estrategia.alcance }"
						placeholder="Alcance" id="alcance" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			
				
				
			
			<form:hidden id="idEstrategia" path="idEstrategia" value="${estrategia.idEstrategia}" />

			</fieldset>

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>









<c:forEach items="${estrategias}" var="estrategia">
<div id="-${estrategia.idEstrategia}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="estrategiaForm"
			action="/sispai/estrategias/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		        
                    <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> 	Edición de Estrategía</h5></legend>
		     <div class="row">
	            <div class="input-field col s12">
					<form:input path="nombre" value="${estrategia.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			 <div class="row">
			     <div class="input-field col s12">
					<form:input path="descripcion" value="${estrategia.descripcion }"
						placeholder="Descripción..." id="descripcion" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			 
			  <div class="row">
			     <div class="input-field col s12">
					<form:input path="alcance" value="${estrategia.alcance }"
						placeholder="Alcance" id="alcance" type="text" class="validate"
						 required="required" />
				</div>
			 </div>
			
				
				
			
			<form:hidden id="idEstrategia" path="idEstrategia" value="${estrategia.idEstrategia}" />

			
			
	<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>

