
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<!-- Modal Trigger -->
  

<div id="contenido" class="card-panel hoverable">
<c:if test="${createvacunasx23}">
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>

<hr>
	 	<div class="container">
		<c:if test="${readvacunasx23}">
		<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Vacuna</th>
					<th>D�sis</th>
					<th>Edad (meses)</th>
					<th>V�a</th>
					<th>Enfermedad que Previene</th>
					<th>Proporci�n (ml)</th>
					<th width="5%">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${vacunas}" var="vacuna">
					<tr>
						<td>${vacuna.nombre }</td>
						<td>${vacuna.dosis }</td>
						<td>${vacuna.edad }</td>
						<td>${vacuna.via }</td>
						<td>${vacuna.enfermedad }</td>
						<td>${vacuna.proporcion }</td>
						<td width="5%">
							<c:if test="${updatevacunasx23}">	
									<a class=" modal-trigger" href="#-${vacuna.idVacuna }"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deletevacunasx23}">		
									<a class="" href="/sispai/vacunas/delete/${vacuna.idVacuna }" data-toggle="modal"data-target="#" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		</div>
</div>


<!-- Modal Structure -->
<div id="agregar" class="modal white darken-4">
	<div class="modal-header">
		<!-- 		<h4>Agregar Empresa</h4> -->
	</div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="vacunaForm"
			action="/sispai/vacunas/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
             
             
              <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Registro Vacuna</h5></legend>
			<div class="row">
		
			
			
				<div class="input-field col s12">
					<form:input path="nombre" value="${vacuna.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						 required="required" />

				</div>
			 </div>
				
				<div class="row">
				<div class="input-field col s6">
				
												
					<form:select path="dosis" id="dosis" class="form-control" value="${vacuna.dosis }" required="required">
+				<option value="" disabled selected>Elige una d�sis</option>
+				<option value="�nica">�nica</option>
+				<option value="Recien nacido">Recien nacido</option>
+				<option value="Primera">Primera</option>	
                <option value="Segunda">Segunda</option>
                <option value="Tercera">Tercera</option>
                <option value="Refuerzo">Refuerzo</option>
                <option value="Primer refuerzo">Primer Refuerzo</option>				
                					
+		             </form:select>
				</div>
				
				<div class="input-field col s6">
				     <label for="proporcion">Proporcion en ml</label>
					<form:input path="proporcion" value="${vacuna.proporcion }"
						placeholder="Proporcion en ml " id="proporcion" type="text" class="validate"
						required="required" />

				</div>
				
			    </div>

			<div class="row">
			<div class="input-field col s12">
				
												
					<form:select path="edad" id="edad" class="form-control" value="${vacuna.edad }" required="required">
+				<option value="" disabled selected>Elige la edad de aplicaci�n</option>
+				<option value="0">Reci�n nacido</option>
+				<option value="2">2 meses</option>
+				<option value="4">4 meses</option>	
                <option value="6">6 meses</option>
                <option value="7">7 meses</option>
                <option value="12">12 meses</option>
                <option value="18">18 meses</option>				
                					
+		             </form:select>
				</div>
			</div>

				<div class="row">
			<div class="input-field col s12">
				
												
					<form:select path="via" id="via" class="form-control" value="${vacuna.via }" required="required">
+				<option value="" disabled selected>Elige una via de aplicaci�n</option>
+				<option value="Intrad�rmica">Intrad�rmica</option>
+				<option value="Intramuscular">Intramuscular</option>
+				<option value="Oral">Oral</option>	
                <option value="Subcut�nea">Subcut�nea</option>
         
+		             </form:select>
				</div>
			</div>
			
			<div class="input-field col s12">
					<form:input path="enfermedad" value="${vacuna.enfermedad }"
						placeholder="Enfermedad que previene" id="enfermedad" type="text" class="validate"
						required="required" />

				</div>
			 </div>
			
			<form:hidden id="idVacuna" path="idVacuna" value="${vacuna.idVacuna}" />

			</fieldset>

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>







<c:forEach items="${vacunas}" var="vacuna">
<div id="-${vacuna.idVacuna}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">

  <form:form method="post" modelAttribute="vacunaForm"
			action="/sispai/vacunas/add"  autocomplete="off" accept-charset="ISO-8859-1">
			
		     <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Edici�n Vacuna</h5></legend>
			<div class="row">
		
			
			
				<div class="input-field col s12">
					<form:input path="nombre" value="${vacuna.nombre }"
						placeholder="Nombre" id="nombre" type="text" class="validate"
						required="required" />

				</div>
			 </div>
				
				<div class="row">
				<div class="input-field col s6">
				
												
					<form:select path="dosis" id="dosis" class="form-control" value="${vacuna.dosis }" required="required">
+				<option value="" disabled selected>Elige una d�sis</option>
+				<option value="�nica">�nica</option>
+				<option value="Recien nacido">Recien nacido</option>
+				<option value="Primera">Primera</option>	
                <option value="Segunda">Segunda</option>
                <option value="Tercera">Tercera</option>
                <option value="Refuerzo">Refuerzo</option>
                <option value="Primer refuerzo">Primer Refuerzo</option>				
                					
+		             </form:select>
				</div>
				
				<div class="input-field col s6">
				     <label for="proporcion">Proporcion en ml</label>
					<form:input path="proporcion" value="${vacuna.proporcion }"
						placeholder="Proporcion en ml " id="proporcion" type="text" class="validate"
						required="required" />

				</div>
				
			    </div>

			<div class="row">
			<div class="input-field col s12">
				
												
					<form:select path="edad" id="edad" class="form-control" value="${vacuna.edad }" required="required">
+				<option value="" disabled selected>Elige la edad de aplicaci�n</option>
+				<option value="0">Reci�n nacido</option>
+				<option value="2">2 meses</option>
+				<option value="4">4 meses</option>	
                <option value="6">6 meses</option>
                <option value="7">7 meses</option>
                <option value="12">12 meses</option>
                <option value="18">18 meses</option>				
                					
+		             </form:select>
				</div>
			</div>

				<div class="row">
			<div class="input-field col s12">
				
												
					<form:select path="via" id="via" class="form-control" value="${vacuna.via }" required="required">
+				<option value="" disabled selected>Elige una via de aplicaci�n</option>
+				<option value="Intrad�rmica">Intrad�rmica</option>
+				<option value="Intramuscular">Intramuscular</option>
+				<option value="Oral">Oral</option>	
                <option value="Subcut�nea">Subcut�nea</option>
         
+		             </form:select>
				</div>
			</div>
			
			<div class="input-field col s12">
					<form:input path="enfermedad" value="${vacuna.enfermedad }"
						placeholder="Enfermedad que previene" id="enfermedad" type="text" class="validate"
						required="required" />

				</div>
			 </div>
			
			<form:hidden id="idVacuna" path="idVacuna" value="${vacuna.idVacuna}" />

			</fieldset>	
			
			
			
			
			
	<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>

</c:forEach>

