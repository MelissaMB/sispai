package com.sisbam.sispai.dao;

import java.util.List;
import java.util.Map;

import com.sisbam.sispai.entity.administration.CanvasjsChartData;
 

 
public class CanvasjsChartDaoImpl implements CanvasjsChartDao {
 
	@Override
	public List<List<Map<Object, Object>>> getCanvasjsChartData() {
		return CanvasjsChartData.getCanvasjsDataList();
	}
 
}                        