package com.sisbam.sispai.configuration;

import org.springframework.security.web.context.*;

/* 
 * Esta clase registra el: security filter chain
 * En lugar de hacerlo en la clase SpringMVCInitializer
 */
public class SecurityWebApplicationInitializer
	extends AbstractSecurityWebApplicationInitializer {

}
