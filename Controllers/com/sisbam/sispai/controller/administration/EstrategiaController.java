package com.sisbam.sispai.controller.administration;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;

import com.sisbam.sispai.entity.administration.Empresa;
import com.sisbam.sispai.entity.administration.Estrategia;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Vacuna;
import com.sisbam.sispai.entity.security.Permisos;


@Controller
public class EstrategiaController  {
	

	@Autowired
	private DaoImp manage_entity;//generalizacion de todo lo que tenga que ver con SQL
	
	private String path = "Administration/Estrategia/";//ruta donde esta la carpeta de las vista
	private static final String IDENTIFICADOR = "estrategiax23";
	
	private Permisos permisos;//permisos del usuario en determinada vista

	
//Metodo para leer de la base de datos responde con una lista de vacunas la URL:/vacunas
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/estrategias", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/estrategias", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())//si no tiene permiso de leer mandara a la pantalla de error 403 Forbiden
		{
			Estrategia estrategia = new Estrategia();
			model.addAttribute("estrategiaForm", estrategia);
			model.addAttribute("estrategia", null);
				List<Estrategia> estrategias = (List<Estrategia>) this.manage_entity.getAll("Estrategia");
				model.addAttribute("estrategias", estrategias);
			retorno = path+"estrategia";
		}
		return retorno;
	}
	
	@RequestMapping(value = "/estrategias/add", method = RequestMethod.POST)
	public String saveOrUpadateEstrategia(@ModelAttribute("estrategiaForm") Estrategia estrategiaRecibida,Model model) throws ClassNotFoundException {
		String retorno = "403";
		if(permisos.isC())
		{
			Estrategia estrategia = estrategiaRecibida;
				if(estrategia.getIdEstrategia()==0){
					manage_entity.save(Estrategia.class.getName(), estrategia);
				}else{
					manage_entity.update(Estrategia.class.getName(), estrategia);
				}
				retorno="redirect:/estrategias";
		}
		return retorno;
	}
	
	
	@RequestMapping(value = "/estrategias/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String idEstrategia, Model model, HttpServletRequest request) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isU()) 
		{
			Estrategia estrategia = (Estrategia) manage_entity.getById(Estrategia.class.getName(), Integer.parseInt(idEstrategia));
			model.addAttribute("estrategia", estrategia);
			Estrategia estrategiaForm = new Estrategia();
			model.addAttribute("estrategiaForm", estrategiaForm);
			retorno=path+"estrategia-form";
		}
		
		return retorno;
	}
	
	/*
	 * Delete:
	 * Borra objetos de tipo vacuna mediante un id enviado en la URL. 
	 */


	@RequestMapping(value = "/estrategias/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String idEstrategia, Model model) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isD()) {
		Estrategia estrategia = (Estrategia) manage_entity.getById(Estrategia.class.getName(), Integer.parseInt(idEstrategia));
		manage_entity.delete(Estrategia.class.getName(), estrategia);
		model.addAttribute("estrategia", estrategia);
		
		Estrategia estrategiaForm = new Estrategia();
		model.addAttribute("estrategiaForm", estrategiaForm);
		retorno="redirect:/estratategias ";
		}
		return retorno;
	}
	
	
}

