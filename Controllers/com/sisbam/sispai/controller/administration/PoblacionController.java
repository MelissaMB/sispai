package com.sisbam.sispai.controller.administration;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Actividad;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Departamento;
import com.sisbam.sispai.entity.administration.Jornada;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Zona;
import com.sisbam.sispai.entity.security.Permisos;

@Controller
public class PoblacionController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Poblacion/";
	private static final String IDENTIFICADOR = "poblacionS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/poblaciones/{id}", method =  RequestMethod.GET)
	public String Jornadas(@PathVariable("id") String idDepartamento, Model model, HttpServletRequest request) throws ClassNotFoundException {
String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/planes", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
		String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(usuario);
		
		
		if(permisos.isR())
		{
			Municipio municipio = new Municipio();
			model.addAttribute("municipioForm", municipio);
			model.addAttribute("municipio", null);
		
		
	
			
		List<Municipio> municipios = (List<Municipio>) this.manage_entity.getListByName("Municipio", "idDepartamento", idDepartamento);
		model.addAttribute("municipios", municipios);
		retorno=path+"poblacion";
		}
		return retorno;
	}
	

	
	
	
	
	//GUARDAR
		@RequestMapping(value = "/poblaciones/add", method = {RequestMethod.POST, RequestMethod.GET})
		
		public String saveOrUpadatePoblacion(@ModelAttribute("poblacionForm") Municipio muncipioRecibido , Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
				   Municipio municipio = muncipioRecibido;
				
				  
					if(municipio.getIdMunicipio()==0) {
						manage_entity.save(Actividad.class.getName(), municipio);
					//	manage_entity.save(Recurso.class.getName(), recurso);
					}else{
						manage_entity.update(Municipio.class.getName(), municipio);
					//	manage_entity.update(Recurso.class.getName(), recurso);
					}
					retorno="redirect:/poblaciones";
			}
			return retorno;
		}
		
		//ACTUALIZAR
		@RequestMapping(value = "/poblaciones/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idMunicipio, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				Municipio municipio = (Municipio) manage_entity.getById(Municipio.class.getName(), Integer.parseInt(idMunicipio));
				model.addAttribute("municipio", municipio);
				Municipio municipioForm = new Municipio();
				model.addAttribute("municipioForm", municipioForm);
				
				
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/poblaciones/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idMunicipio, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
				Municipio municipio = (Municipio) manage_entity.getById(Municipio.class.getName(), Integer.parseInt(idMunicipio));
			manage_entity.delete(Municipio.class.getName(), municipio);
			model.addAttribute("municipio", municipio);
			
			Municipio municipioForm = new Municipio();
			model.addAttribute("municipioForm", municipioForm);
			retorno="redirect:/poblaciones";
			}
			return retorno;
		}
		
		
}
