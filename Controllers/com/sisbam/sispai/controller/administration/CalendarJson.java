package com.sisbam.sispai.controller.administration;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Calendario;
import com.sisbam.sispai.entity.security.Permisos;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@Controller
public class CalendarJson {
	
	
	
	
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        
	        try {
	            
	            List progs = new ArrayList();
	            
	            Class.forName("com.mysql.jdbc.Driver");
	            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/calendar", "root", "");
	            ResultSet rs = con.createStatement().executeQuery("SELECT `id`, `title`, `start`, `end` FROM `events`");
	            
	            while (rs.next()) {
	                Calendario pb = new Calendario();
	                pb.setId(Integer.parseInt(rs.getString(1)));
	                pb.setStart(rs.getString(3));
	                pb.setEnd(rs.getString(4));
	                pb.setTitle(rs.getString(2));
	                progs.add(pb);
	                
	            }
	            response.setContentType("application/json");
	            response.setCharacterEncoding("UTF-8");
	            PrintWriter out = response.getWriter();
	            out.write(new Gson().toJson(progs));
	            
	           
	            
	        } catch (ClassNotFoundException ex) {
	            Logger.getLogger(CalendarJson.class.getName()).log(Level.SEVERE, null, ex);
	        } catch (SQLException ex) {
	            Logger.getLogger(CalendarJson.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	    }
	    
	}
