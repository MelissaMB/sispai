package com.sisbam.sispai.controller.administration;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Actividad;
import com.sisbam.sispai.entity.administration.Calendario;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Jornada;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Vacuna;
import com.sisbam.sispai.entity.security.Permisos;




@Controller
public class JornadaController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Jornada/";
	private static final String IDENTIFICADOR = "jornadaS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jornadas", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/jornadas", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
		String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(usuario);
		
		
		if(permisos.isR())
		{

			Jornada jornada = new Jornada();
			model.addAttribute("jornadaForm", jornada);
			model.addAttribute("jornada", null);
	
			List<Jornada> jornadas = (List<Jornada>) this.manage_entity.getAll("Jornada");
			List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
			List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
		
			
			model.addAttribute("jornadas", jornadas);
			model.addAttribute("responsables", responsables);
			model.addAttribute("planes", planes);
		
			
			retorno = path+"jornada";
		}
		return retorno;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jornada/add", method = {RequestMethod.POST, RequestMethod.GET})
	public String addJornada(Model model, HttpServletRequest request)  {
		
		
		String retorno = "403";
		if(permisos.isC()){
			Jornada jornada = new Jornada();

			List<Jornada> jornadas = (List<Jornada>) this.manage_entity.getAll("Jornada");
			List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
			List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
		
			model.addAttribute("jornadaForm", jornada);
			model.addAttribute("jornada", null);
			model.addAttribute("jornadas", jornadas);
			model.addAttribute("responsables", responsables);
			model.addAttribute("planes", planes);
		
	
			
			retorno = path+"jornada-form";
		}
		return retorno;
		
	}
	
	
	//GUARDAR
		@RequestMapping(value = "/jornadas/add", method = {RequestMethod.POST, RequestMethod.GET})
		public String saveOrUpadateJornada(@ModelAttribute("jornadaForm") Jornada jornadaRecibida, Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{      
				   Jornada jornada = jornadaRecibida;
				   Responsable responsableRecibido = (Responsable) this.manage_entity.getById(Responsable.class.getName(), jornada.getIdResponsable());
				   jornada.setResponsable(responsableRecibido);
				   Plan planRecibido = (Plan) this.manage_entity.getById(Plan.class.getName(), jornada.getIdPlan());
				   jornada.setPlan(planRecibido);
				
				
					
					if(jornada.getIdJornada()==0) {
						manage_entity.save(Jornada.class.getName(), jornada);
					}else{
						manage_entity.update(Jornada.class.getName(), jornada);
					}
					retorno="redirect:/jornadas";
			}
			return retorno;
		}
		

		
		//ACTUALIZAR
		@RequestMapping(value = "/jornadas/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idJornada, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				Jornada jornada = (Jornada) manage_entity.getById(Jornada.class.getName(), Integer.parseInt(idJornada));
				model.addAttribute("jornada", jornada);
				
				Jornada jornadaForm = new Jornada();
				model.addAttribute("jornadaForm", jornadaForm);
				List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
				List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
				
				model.addAttribute("responsables", responsables);
				model.addAttribute("planes", planes);
			
		
				retorno=path+"jornada-form";
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/jornadas/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idJornada, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			Jornada jornada = (Jornada) manage_entity.getById(Jornada.class.getName(), Integer.parseInt(idJornada));
			manage_entity.delete(Plan.class.getName(), jornada);
			model.addAttribute("jornada", jornada);
			
			Jornada jornadaForm = new Jornada();
			model.addAttribute("jornadaForm", jornadaForm);
			retorno="redirect:/jornadas";
			}
			return retorno;
		}
		
		
		@SuppressWarnings("unchecked")
		@RequestMapping(value = "/jornadas/calendar/{id}", method ={RequestMethod.POST, RequestMethod.GET})
		 public String calendar(@PathVariable("id") String idPlan, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, SQLException
		 {
			 String retorno = "403";
				
				HttpSession session = request.getSession();
				ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
				session = facilitador.Obtener("/sispai/jornadas", request, manage_entity,IDENTIFICADOR);
				permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
				String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				System.out.println(usuario);
				
				if(permisos.isC()){
					  List progs = new ArrayList();
					
					  Class.forName("org.postgresql.Driver");
			            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5433/paidb", "igfuser", "administrador");
			            ResultSet rs = con.createStatement().executeQuery("SELECT idjornada, fechainicio, fechafin, nombre FROM jornada where idplan=2");
			            System.out.println(rs);
						
						/*List<String> rs = new ArrayList<String>();
						String sql = "SELECT idJornada, nombre, fechaInicio, fechaFin FROM jornada";
						rs = (List<String>) manage_entity.executeNativeQuery(sql);	
						
			            while (rs.next()) {
			                Jornada pb = new Jornada();
			                pb.setIdJornada(Integer.parseInt(rs.getString(1)));
			                pb.setFechaInicio(rs.getString(3));
			                pb.setFechaFin(rs.getString(4));
			                pb.setNombre(rs.getString(2));
			                progs.add(pb);
			                
			            	System.out.println(progs);
			            }
			            response.setContentType("application/json");
			            response.setCharacterEncoding("UTF-8");
			            PrintWriter out = response.getWriter();
			            out.write(new Gson().toJson(progs));*/
					
			           
					
			
					  retorno = path+"calendar";
		            }
			
				
				return retorno;
				
			
		 }
				
		 
				
				
				
				
		 }
		


		
	
	
		
		