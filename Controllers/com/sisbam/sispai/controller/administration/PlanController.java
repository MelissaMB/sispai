package com.sisbam.sispai.controller.administration;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Departamento;
import com.sisbam.sispai.entity.administration.Jornada;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Vacuna;
import com.sisbam.sispai.entity.security.Permisos;



@Controller
public class PlanController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Plan/";
	private static final String IDENTIFICADOR = "planS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/planes", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/planes", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
		String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(usuario);
		
		
		if(permisos.isR())
		{

			Plan plan = new Plan();
			model.addAttribute("planForm", plan);
			model.addAttribute("plan", null);
	
			List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
			List<CentroSalud> centros = (List<CentroSalud>) this.manage_entity.getAll("CentroSalud");
			List<Departamento> departamentos = (List<Departamento>) this.manage_entity.getAll("Departamento");
			List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
			List<Vacuna> vacunas = (List<Vacuna>) this.manage_entity.getAll("Vacuna");
			
			model.addAttribute("planes", planes);
			model.addAttribute("centros", centros);
			model.addAttribute("departamentos", departamentos);
			model.addAttribute("municipios", municipios);
			model.addAttribute("vacunas", vacunas);
			
			retorno = path+"plan";
		}
		return retorno;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/plan/add", method = {RequestMethod.POST, RequestMethod.GET})
	public String addProyecto(Model model, HttpServletRequest request)  {
		
		
		String retorno = "403";
		if(permisos.isC()){
			Plan plan = new Plan();
			List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
			List<CentroSalud> centros = (List<CentroSalud>) this.manage_entity.getAll("CentroSalud");
			List<Departamento> departamentos = (List<Departamento>) this.manage_entity.getAll("Departamento");
			List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
			List<Vacuna> vacunas = (List<Vacuna>) this.manage_entity.getAll("Vacuna");
			
			model.addAttribute("centros", centros);
			model.addAttribute("planForm", plan);
			model.addAttribute("plan", null);
			model.addAttribute("planes", planes);
			model.addAttribute("departamentos", departamentos);
			model.addAttribute("municipios", municipios);
			model.addAttribute("vacunas", vacunas);
			
			
			retorno = path+"plan-form";
		}
		return retorno;
		
	}
	
	
	//GUARDAR
		@RequestMapping(value = "/planes/add", method = {RequestMethod.POST, RequestMethod.GET})
		public String saveOrUpadateCentro(@ModelAttribute("planForm") Plan planRecibido, Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
				    Plan plan = planRecibido;
					CentroSalud centroRecibido = (CentroSalud) this.manage_entity.getById(CentroSalud.class.getName(), plan.getIdCentro());
					plan.setCentro(centroRecibido);
					
					Departamento departamentoRecibido = (Departamento) this.manage_entity.getById(Departamento.class.getName(), plan.getIdDepartamento());
					plan.setDepartamento(departamentoRecibido);
					
					Municipio municipioRecibido = (Municipio) this.manage_entity.getById(Municipio.class.getName(), plan.getIdMunicipio());
					plan.setMunicipio(municipioRecibido);
					
					Vacuna vacunaRecibida = (Vacuna) this.manage_entity.getById(Vacuna.class.getName(), plan.getIdVacuna());
					plan.setVacuna(vacunaRecibida);
					
					if(plan.getIdPlan()==0) {
						manage_entity.save(Plan.class.getName(), plan);
					}else{
						manage_entity.update(Plan.class.getName(), plan);
					}
					retorno="redirect:/planes";
			}
			return retorno;
		}
		

		
		//ACTUALIZAR
		@RequestMapping(value = "/planes/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idPlan, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				Plan plan = (Plan) manage_entity.getById(Plan.class.getName(), Integer.parseInt(idPlan));
				model.addAttribute("plan", plan);
				
				Plan planForm = new Plan();
				model.addAttribute("planForm", planForm);
				
				List<CentroSalud> centros = (List<CentroSalud>) this.manage_entity.getAll("CentroSalud");
				model.addAttribute("centros", centros);
				
				List<Departamento> departamentos = (List<Departamento>) this.manage_entity.getAll("Departamento");
				List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
				List<Vacuna> vacunas = (List<Vacuna>) this.manage_entity.getAll("Vacuna");
				
				model.addAttribute("departamentos", departamentos);
				model.addAttribute("municipios", municipios);
				model.addAttribute("vacunas", vacunas);
				
			
				retorno=path+"plan-form";
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/planes/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idPlan, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			Plan plan = (Plan) manage_entity.getById(Plan.class.getName(), Integer.parseInt(idPlan));
			manage_entity.delete(Plan.class.getName(), plan);
			model.addAttribute("plan", plan);
			
			Plan planForm = new Plan();
			model.addAttribute("planForm", planForm);
			retorno="redirect:/planes";
			}
			return retorno;
		}
		
		
		//Mostrar las jornadas filtradas por planes
		
		@RequestMapping(value = "/plan-jornada/{id}", method =  RequestMethod.GET)
		public String Jornadas(@PathVariable("id") String idPlan, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			double estado=0;
			if(permisos.isD()) {
			
			Plan plan = (Plan) manage_entity.getById(Plan.class.getName(), Integer.parseInt(idPlan));
			model.addAttribute("plan", plan);
				
			List<Jornada> jornadas = (List<Jornada>) this.manage_entity.getListByName("Jornada", "idPlan", idPlan);
			model.addAttribute("jornadas", jornadas);
			retorno=path+"plan-jornadas";
			}
			return retorno;
		}
		

		
}