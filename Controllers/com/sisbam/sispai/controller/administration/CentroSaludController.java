package com.sisbam.sispai.controller.administration;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Departamento;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Zona;
import com.sisbam.sispai.entity.security.Permisos;

@Controller
public class CentroSaludController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Centros/";
	private static final String IDENTIFICADOR = "centrosS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/centros", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/centros", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
		String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(usuario);
		
		
		if(permisos.isR())
		{
			CentroSalud centro = new CentroSalud();
			model.addAttribute("centrosForm", centro);
			model.addAttribute("centro", null);
	
			List<CentroSalud> centros = (List<CentroSalud>) this.manage_entity.getAll("CentroSalud");
			List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
			model.addAttribute("centros", centros);
			model.addAttribute("municipios", municipios);
			retorno = path+"centroSalud";
		}
		return retorno;
	}
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/centros/add", method = RequestMethod.GET)
	public String addCentro(Model model, HttpServletRequest request)  {
		
		
		String retorno = "403";
		if(permisos.isC()){
			CentroSalud centro = new CentroSalud();
			List<CentroSalud> centros = (List<CentroSalud>) this.manage_entity.getAll("CentroSalud");
			List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
			model.addAttribute("municipios", municipios);
			model.addAttribute("centrosForm", centro);
			model.addAttribute("centro", null);
			model.addAttribute("centros", centros);
			
			retorno = path+"centroSalud-form";
		}
		return retorno;
		
	}
	
	
	//GUARDAR
		@RequestMapping(value = "/centros/add", method = RequestMethod.POST)
		public String saveOrUpadateCentro(@ModelAttribute("centrosForm") CentroSalud centroRecibido,Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
				    CentroSalud centroSalud = centroRecibido;
					Municipio municipioRecibido = (Municipio) this.manage_entity.getById(Municipio.class.getName(), centroSalud.getIdMunicipio());
					centroSalud.setMunicipio(municipioRecibido);
					
					if(centroSalud.getIdCentro()==0) {
						manage_entity.save(CentroSalud.class.getName(), centroSalud);
					}else{
						manage_entity.update(CentroSalud.class.getName(), centroSalud);
					}
					retorno="redirect:/centros";
			}
			return retorno;
		}
		
		//ACTUALIZAR
		@RequestMapping(value = "/centros/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idCentro, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				CentroSalud centro = (CentroSalud) manage_entity.getById(CentroSalud.class.getName(), Integer.parseInt(idCentro));
				model.addAttribute("centro", centro);
				
				CentroSalud centrosForm = new CentroSalud();
				model.addAttribute("centrosForm", centrosForm);
				
				List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
				model.addAttribute("municipios", municipios);
			
				retorno=path+"centroSalud-form";
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/centros/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idCentro, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			CentroSalud centro = (CentroSalud) manage_entity.getById(CentroSalud.class.getName(), Integer.parseInt(idCentro));
			manage_entity.delete(CentroSalud.class.getName(), centro);
			model.addAttribute("centro", centro);
			
			CentroSalud centrosForm = new CentroSalud();
			model.addAttribute("centrosForm", centrosForm);
			retorno="redirect:/centros";
			}
			return retorno;
		}

		
}