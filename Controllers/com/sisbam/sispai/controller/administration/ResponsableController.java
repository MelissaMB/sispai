package com.sisbam.sispai.controller.administration;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Empresa;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Vacuna;
import com.sisbam.sispai.entity.security.Permisos;


@Controller
public class ResponsableController  {
	
	@Autowired
	private DaoImp manage_entity;//generalizacion de todo lo que tenga que ver con SQL
	
	private String path = "Administration/Responsable/";//ruta donde esta la carpeta de las vista
	private static final String IDENTIFICADOR = "responsbalex23";
	
	private Permisos permisos;//permisos del usuario en determinada vista

	
//Metodo para leer de la base de datos responde con una lista de vacunas la URL:/vacunas
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/responsables", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/responsables", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())//si no tiene permiso de leer mandara a la pantalla de error 403 Forbiden
		{
			Responsable responsable = new Responsable();
			model.addAttribute("responsableForm", responsable);
			model.addAttribute("responsable", null);
				List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
				model.addAttribute("responsables", responsables);
			retorno = path+"responsable";
		}
		return retorno;
	}
	
	@RequestMapping(value = "/responsables/add", method = RequestMethod.POST)
	public String saveOrUpadateResponsable(@ModelAttribute("responsableForm") Responsable responsableRecibido,Model model) throws ClassNotFoundException {
		String retorno = "403";
		if(permisos.isC())
		{
			Responsable responsable = responsableRecibido;
				if(responsable.getIdResponsable()==0){
					manage_entity.save(Responsable.class.getName(), responsable);
				}else{
					manage_entity.update(Responsable.class.getName(), responsable);
				}
				retorno="redirect:/responsables";
		}
		return retorno;
	}
	
	
	@RequestMapping(value = "/responsables/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String idResponsable, Model model, HttpServletRequest request) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isU()) 
		{
			Responsable responsable = (Responsable) manage_entity.getById(Responsable.class.getName(), Integer.parseInt(idResponsable));
			model.addAttribute("responsable", responsable);
			Responsable responsableForm = new Responsable();
			model.addAttribute("responsableForm", responsableForm);
			retorno=path+"responsable-form";
		}
		
		return retorno;
	}
	
	/*
	 * Delete:
	 * Borra objetos de tipo vacuna mediante un id enviado en la URL. 
	 */


	@RequestMapping(value = "/responsables/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String idResponsable, Model model) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isD()) {
		Responsable responsable = (Responsable) manage_entity.getById(Responsable.class.getName(), Integer.parseInt(idResponsable));
		manage_entity.delete(Responsable.class.getName(), responsable);
		model.addAttribute("responsable", responsable);
		
		Responsable responsableForm = new Responsable();
		model.addAttribute("responsableForm", responsableForm);
		retorno="redirect:/responsables";
		}
		return retorno;
	}
	
	
}

