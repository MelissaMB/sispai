package com.sisbam.sispai.controller.administration;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Actividad;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Departamento;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Zona;
import com.sisbam.sispai.entity.security.Permisos;

@Controller
public class ActividadController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Actividad/";
	private static final String IDENTIFICADOR = "actividadS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/actividades", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/actividades", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())
		{
			Actividad actividad = new Actividad();
			model.addAttribute("actividadesForm", actividad);
			model.addAttribute("actividad", null);

			List<Actividad> actividades = (List<Actividad>) this.manage_entity.getAll("Actividad");
			List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
			List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
			
	
				model.addAttribute("actividades", actividades);
				model.addAttribute("responsables",responsables);
				model.addAttribute("planes",planes);
				
			retorno = path+"actividad";
		}
		return retorno;
	}
	

	
	
	
	
	//GUARDAR
		@RequestMapping(value = "/actividades/add", method = {RequestMethod.POST, RequestMethod.GET})
		
		public String saveOrUpadateActividad(@ModelAttribute("actividadForm") Actividad actividadRecibida , Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
				   Actividad actividad = actividadRecibida;
				   //Recurso recurso=recursoRecibido;
				   
				    Responsable responsableRecibido = (Responsable) this.manage_entity.getById(Responsable.class.getName(), actividad.getIdResponsable());
				   actividad.setResponsable(responsableRecibido);
				   Plan planRecibido = (Plan) this.manage_entity.getById(Plan.class.getName(), actividad.getIdPlan());
				   actividad.setPlan(planRecibido);
				
				 //   List<Recurso>recursos= (List<Recurso>)this.manage_entity.getInstancesById("recurso", "actividad", "idactividad", actividad.getIdActividad());
				  //  actividad.setRecurso(recursos);
				  
					if(actividad.getIdActividad()==0) {
						manage_entity.save(Actividad.class.getName(), actividad);
					//	manage_entity.save(Recurso.class.getName(), recurso);
					}else{
						manage_entity.update(Actividad.class.getName(), actividad);
					//	manage_entity.update(Recurso.class.getName(), recurso);
					}
					retorno="redirect:/actividades";
			}
			return retorno;
		}
		
		//ACTUALIZAR
		@RequestMapping(value = "/actividades/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idActividad, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				Actividad actividad = (Actividad) manage_entity.getById(Actividad.class.getName(), Integer.parseInt(idActividad));
				model.addAttribute("actividad", actividad);
				Actividad actividadForm = new Actividad();
				model.addAttribute("actividadForm", actividadForm);
				
				List<Responsable> responsables = (List<Responsable>) this.manage_entity.getAll("Responsable");
				model.addAttribute("responsables",responsables);
				
				List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
				model.addAttribute("planes",planes);
				retorno=path+"actividad-form";
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/actividades/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idActividad, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			Actividad actividad = (Actividad) manage_entity.getById(Actividad.class.getName(), Integer.parseInt(idActividad));
			manage_entity.delete(Actividad.class.getName(), actividad);
			model.addAttribute("actividad", actividad);
			
			Actividad actividadForm = new Actividad();
			model.addAttribute("actividadForm", actividadForm);
			retorno="redirect:/actividades";
			}
			return retorno;
		}
		
		
}
