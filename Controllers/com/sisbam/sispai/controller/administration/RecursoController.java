package com.sisbam.sispai.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Actividad;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Recurso;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.security.Permisos;

@Controller
public class RecursoController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Recurso/";
	private static final String IDENTIFICADOR = "recursoS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/recursos", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/recursos", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())
		{
			Recurso recurso = new Recurso();
			model.addAttribute("recursoForm", recurso);
			model.addAttribute("recurso", null);

			List<Actividad> actividades = (List<Actividad>) this.manage_entity.getAll("Actividad");
			List<Recurso> recursos = (List<Recurso>) this.manage_entity.getAll("Recurso");
			
			
	
				model.addAttribute("actividades", actividades);
				model.addAttribute("recursos",recursos);
				
				
			retorno = path+"recurso";
		}
		return retorno;
	}
	

	
	
	
	
	//GUARDAR
		@RequestMapping(value = "/recursos/add", method = {RequestMethod.POST, RequestMethod.GET})
		
		public String saveOrUpadateRecurso(@ModelAttribute("recursoForm") Recurso recursoRecibido , Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
				   Recurso recurso = recursoRecibido;
				   //Recurso recurso=recursoRecibido;
				   
				   Actividad activdadRecibida = (Actividad) this.manage_entity.getById(Actividad.class.getName(), recurso.getIdActividad());
				   recurso.setActividad(activdadRecibida);
				  
				  
					if(recurso.getIdRecurso()==0) {
						manage_entity.save(Recurso.class.getName(), recurso);
					//	manage_entity.save(Recurso.class.getName(), recurso);
					}else{
						manage_entity.update(Recurso.class.getName(), recurso);
					//	manage_entity.update(Recurso.class.getName(), recurso);
					}
					retorno="redirect:/recursos";
			}
			return retorno;
		}
		
	
		
		//ELIMINAR
		@RequestMapping(value = "/recursos/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idRecurso, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			Recurso recurso = (Recurso) manage_entity.getById(Recurso.class.getName(), Integer.parseInt(idRecurso));
			manage_entity.delete(Recurso.class.getName(), recurso);
			model.addAttribute("recurso", recurso);
			
			Recurso recursoForm = new Recurso();
			model.addAttribute("recursoForm", recursoForm);
			retorno="redirect:/recursos";
			}
			return retorno;
		}
		
		
}
