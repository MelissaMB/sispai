package com.sisbam.sispai.controller.administration;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Actividad;
import com.sisbam.sispai.entity.administration.CentroSalud;
import com.sisbam.sispai.entity.administration.Departamento;
import com.sisbam.sispai.entity.administration.Municipio;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.administration.Responsable;
import com.sisbam.sispai.entity.administration.Zona;
import com.sisbam.sispai.entity.security.Permisos;

@Controller
public class DepartamentoController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Poblacion/";
	private static final String IDENTIFICADOR = "departamentoS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/departamentos", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/departamentos", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())
		{   int poblacion=0;
			Departamento departamento = new Departamento();
			model.addAttribute("departamentoForm", departamento);
			model.addAttribute("departamento", null);

			List<Departamento> departamentos = (List<Departamento>) this.manage_entity.getAll("Departamento");
			List<Municipio> municipios = (List<Municipio>) this.manage_entity.getAll("Municipio");
			
			for(Municipio pobla: municipios)
			{
				poblacion= poblacion+ pobla.getPoblacion();
			}
	
				model.addAttribute("departamentos", departamentos);
				model.addAttribute("poblacion", poblacion);
			
				
			retorno = path+"departamento";
		}
		return retorno;
	}
	

	
	
	
		
}