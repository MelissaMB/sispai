package com.sisbam.sispai.controller.administration;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Jornada;
import com.sisbam.sispai.entity.administration.Plan;
import com.sisbam.sispai.entity.security.Permisos;

@Controller


public class ChartController {
	
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Graficos/";
	private static final String IDENTIFICADOR = "graficoS";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	

		
		@RequestMapping(value = "/chart", method = RequestMethod.GET)
		 public String gantt(Model model, HttpServletRequest request)
		 {
			 String retorno = "403";
				
				HttpSession session = request.getSession();
				ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
				session = facilitador.Obtener("/sispai/chart", request, manage_entity,IDENTIFICADOR);
				permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
				String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				System.out.println(usuario);
				
				if(permisos.isC()){
				
                      List<Plan> planes = (List<Plan>) this.manage_entity.getAll("Plan");
			          model.addAttribute("planes", planes);
					
					
					List<Plan> nombres = new ArrayList<Plan>();	
					String query ="select nombre from plan";
					nombres = (List<Plan>) manage_entity.executeNativeQuery(query);
					model.addAttribute("nombres", nombres);
					
					
					List<Plan> personas = new ArrayList<Plan>();	
					String query1 ="select personas from plan";
					personas = (List<Plan>) manage_entity.executeNativeQuery(query1);
					model.addAttribute("personas", personas);
				
				retorno = path+"chart";
				}
				
			 return retorno;
		 } 
		
	
}
	
	
