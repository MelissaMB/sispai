package com.sisbam.sispai.controller.administration;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sispai.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sispai.dao.DaoImp;
import com.sisbam.sispai.entity.administration.Empresa;
import com.sisbam.sispai.entity.administration.Vacuna;
import com.sisbam.sispai.entity.security.Permisos;


@Controller
public class VacunaController  {
	
	@Autowired
	private DaoImp manage_entity;//generalizacion de todo lo que tenga que ver con SQL
	
	private String path = "Administration/Vacuna/";//ruta donde esta la carpeta de las vista
	private static final String IDENTIFICADOR = "vacunasx23";
	
	private Permisos permisos;//permisos del usuario en determinada vista

	
//Metodo para leer de la base de datos responde con una lista de vacunas la URL:/vacunas
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vacunas", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sispai/vacunas", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())//si no tiene permiso de leer mandara a la pantalla de error 403 Forbiden
		{
			Vacuna vacuna = new Vacuna();
			model.addAttribute("vacunaForm", vacuna);
			model.addAttribute("vacuna", null);
				List<Vacuna> vacunas = (List<Vacuna>) this.manage_entity.getAll("Vacuna");
				model.addAttribute("vacunas", vacunas);
			retorno = path+"vacuna";
		}
		return retorno;
	}
	
	@RequestMapping(value = "/vacunas/add", method = RequestMethod.POST)
	public String saveOrUpadateEmpresa(@ModelAttribute("vacunaForm") Vacuna vacunaRecibida,Model model) throws ClassNotFoundException {
		String retorno = "403";
		if(permisos.isC())
		{
				Vacuna vacuna = vacunaRecibida;
				if(vacuna.getIdVacuna()==0){
					manage_entity.save(Vacuna.class.getName(), vacuna);
				}else{
					manage_entity.update(Vacuna.class.getName(), vacuna);
				}
				retorno="redirect:/vacunas";
		}
		return retorno;
	}
	
	
	@RequestMapping(value = "/vacunas/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String idVacuna, Model model, HttpServletRequest request) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isU()) 
		{
			Vacuna vacuna = (Vacuna) manage_entity.getById(Vacuna.class.getName(), Integer.parseInt(idVacuna));
			model.addAttribute("vacuna", vacuna);
			Vacuna vacunaForm = new Vacuna();
			model.addAttribute("vacunaForm", vacunaForm);
			retorno=path+"vacuna-form";
		}
		
		return retorno;
	}
	
	/*
	 * Delete:
	 * Borra objetos de tipo vacuna mediante un id enviado en la URL. 
	 */


	@RequestMapping(value = "/vacunas/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String idVacuna, Model model) throws ClassNotFoundException {
		String retorno="403";
		if(permisos.isD()) {
		Vacuna vacuna = (Vacuna) manage_entity.getById(Vacuna.class.getName(), Integer.parseInt(idVacuna));
		manage_entity.delete(Vacuna.class.getName(), vacuna);
		model.addAttribute("vacuna", vacuna);
		
		Vacuna vacunaForm = new Vacuna();
		model.addAttribute("vacunaForm", vacunaForm);
		retorno="redirect:/vacunas";
		}
		return retorno;
	}
	
	
}
